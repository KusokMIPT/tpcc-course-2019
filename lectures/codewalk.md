# Lock-free Allocator

- CAS - `DoCas`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L132

- Классы для маленьких аллокаций: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L183

https://github.com/catboost/catboost/blob/master/library/lfalloc/lf_allocX64.h

- Аллокация `LFAllocImpl`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1410
- Деаллокация `LFFree`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1487

### Пулы для классов

- Lock-free стэк / free list – `LFAllocFreeList`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L572
- Сами списки: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L668

### TL кэш

- Локальный кэш блоков `TThreadAllocInfo`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1004
- В `LFAllocImpl` сначала идем в него: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1434

- Аллокация TL кэша: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1184

Тут используется `pthread_key_create`, чтобы зарегистрировать деструктор для этого кэша, который вернет память в глобальный пул
Кроме того, нужно выбрать, кто именно будет получать ключ и регистрировать деструктор.

- Извлекаем блок из локального кэша в `LFAllocImpl`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1448

### Батчинг при взаимодействии с глобальным пулом

- Оптимизация: батчим трансферы блоков между TL кэшом и глобальным пулом/стеком:
- Забираем блоки в аллокации (при наполнении кэша): https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L866 
- Возвращаем блоки (при переполнении кэша): https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L884
- Появляется отдельный пул для блоков-контейнеров: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L670

### Пул / стек пустой

В TL кэше и в глобальном пуле ничего не нашлось
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1460

Идем в `LFAllocNoCacheMultiple`:
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1460

Сначала пробуем `LFAllocFromCurrentChunk`, уже затем `SlowLFAlloc`

1) `LFAllocFromCurrentChunk`
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L775

Для каждого размера есть текущий _чанк_, от которого мы откусываем блоки, которые затем помещаются в локальный кэш:

- `globalCurrentPtr[N_SIZES]` – храним для каждого класса аллокации поинтер на позицию в текущем чанке

- `LFAllocFromCurrentChunk`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L775
Вычисляем конец чанка, пробуем с помощью `DoCas` передвинуть вперед курсор и тем самым захватить из него кусок

2) Если не получается, то `SlowLFAlloc`:
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L798

Берем блокировку, повторно пробуем выполнить `LFAllocFromCurrentChunk`, вдруг нам повезет, и другой поток уже аллоцировал чанк для нужного нам класса аллокации
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L802

Пробуем взять _чанк_ из пула свободных чанков: 
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L808

Если не получилось, то аллоцируем память, распиливаем на чанки и кладем в пул чанков:
https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L824

### Откуда мы узнаем размер блока в `LFFree`?

Все блоки в конце концов получаются нарезанием чанков, причем каждый чанк используется для одного размера блока.

При захвате чанка в `SlowLFAlloc` будем сохранять размер класса, к которому тот привязан: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L814

При освобождении блока вычислим индекс чанка и посмотрим в глобальный массив: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1506


### Аллокация больших блоков

Функции `LargeBlockAlloc` и `LargeBlockFree`

Деления на классы нет. Пул свободных больших блоков – lock-free хэш-таблица фиксированного размера: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L441

- `LargeBlockAlloc`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L445
- `LargeBlockFree`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L504

### Счетчики

Пример: считаем объем аллоцированной памяти в `LFAllocImpl`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1416

- `IncrementCounter`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1086
- Локальные счетчики в TL кэше: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L1010
- `TLocalCounter`: https://github.com/catboost/catboost/blob/0.11.2/library/lfalloc/lf_allocX64.h#L912
