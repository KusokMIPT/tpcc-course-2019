import json

def find_benchmark(report, name):
    for benchmark in report["benchmarks"]:
        if benchmark["name"].startswith(name):
            return benchmark

    raise RuntimeError("Benchmark not found: {}".format(name))

def get_uncontended_scores(report):
    return find_benchmark(report, "BM_MutexUncontended")

def check_uncontended_scores(report, private_report):
    your_scores = get_uncontended_scores(report)
    private_scores = get_uncontended_scores(private_report)

    return your_scores["real_time"] < private_scores["real_time"] * 3

def check_scores(report, private_report):
    if not check_uncontended_scores(report, private_report):
        return False, "Solution is too slow in uncontended benchmark"

    return True, "OK"

