## SpinLock

Вам дана реализация простейшего Test-and-Set спинлока.

Но в ней не хватает атомарных операций – `AtomicStore` и `AtomicExchange`. 

Реализуйте их!

---

В файле `atomics.S` вам нужно заполнить реализации функций `AtomicStore` и `AtomicExchange`.

У этих функций по два аргумента, они передаются через регистры `rdi` и `rsi`. Функции возвращают результат через регистр `rax`.

В файле `spinlock.hpp` вам нужно заполнить реализацию метода `TryLock`.

----

Для справки см. [Introduction to X86-64 Assembly for Compiler Writers](https://www3.nd.edu/~dthain/courses/cse40243/fall2015/intel-intro.html)