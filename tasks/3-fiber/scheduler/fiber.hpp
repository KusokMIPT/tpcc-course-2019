#pragma once

#include "context.hpp"
#include "stack.hpp"

#include <twist/support/intrusive_list.hpp>

#include <functional>

namespace fiber {

//////////////////////////////////////////////////////////////////////

using FiberRoutine = std::function<void()>;

using FiberId = size_t;

enum class FiberState { Starting, Runnable, Running, Suspended, Terminated };

class Fiber : public IntrusiveListNode<Fiber> {
 public:
  size_t Id() const {
    return id_;
  }

  const FiberStack& Stack() const {
    return stack_;
  }

  ExecutionContext& Context() {
    return context_;
  }

  FiberState State() const {
    return state_;
  }

  void SetState(FiberState target) {
    state_ = target;
  }

  std::uintptr_t* LocalStorage() {
    return stack_.LocalStorage();
  }

  FiberRoutine UserRoutine() {
    return routine_;
  }

  static Fiber* Create(FiberRoutine routine);
  static void SetupTrampoline(Fiber* fiber);

 private:
  FiberStack stack_;
  ExecutionContext context_;
  FiberState state_;
  FiberRoutine routine_;
  FiberId id_;
};

}  // namespace fiber
