cmake_minimum_required(VERSION 3.5)

enable_language(ASM)

begin_task()
set_task_sources(
  context.hpp
  context.cpp
  context.S
  fiber.hpp
  fiber.cpp
  mutex.hpp
  mutex.cpp
  scheduler.hpp
  scheduler.cpp
  stack.hpp
  stack.cpp)
add_task_test(test test.cpp)
end_task()
