#include "scheduler.hpp"

namespace fiber {

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler(size_t threads) {
  // Not implemented
}

void Scheduler::Submit(FiberRoutine routine) {
  // Not implemented
}

size_t Scheduler::ThreadCount() const {
  return 0;  // Not implemented
}

void Scheduler::Shutdown() {
  // Not implemented
}

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber() {
  return nullptr;  // Not implemented
}

//////////////////////////////////////////////////////////////////////

void Spawn(FiberRoutine routine) {
  // Not implemented
}

void Yield() {
  // Not implemented
}

void Terminate() {
  // Not implemented
}

}  // namespace fiber

