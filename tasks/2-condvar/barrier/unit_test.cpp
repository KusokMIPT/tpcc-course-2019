#include "cyclic_barrier.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/threading/stdlike.hpp>

TEST_SUITE(CyclicBarrier) {
  SIMPLE_T_TEST(OneThread) {
    solutions::CyclicBarrier barrier{1};

    for (size_t i = 0; i < 10; ++i) {
      barrier.PassThrough();
    }
  }

  SIMPLE_T_TEST(TwoThreads) {
    solutions::CyclicBarrier barrier{2};

    int my = 0;
    int that = 0;

    auto that_routine = [&]() {
      that = 1;
      barrier.PassThrough();
      ASSERT_EQ(my, 1);
      barrier.PassThrough();
      that = 2;
      barrier.PassThrough();
      ASSERT_EQ(my, 2);
    };

    twist::th::thread that_thread(that_routine);

    my = 1;
    barrier.PassThrough();
    ASSERT_EQ(that, 1);
    barrier.PassThrough();
    my = 2;
    barrier.PassThrough();
    ASSERT_EQ(that, 2);

    that_thread.join();
  }

  SIMPLE_T_TEST(Runners) {
    static const size_t kThreads = 10;
    solutions::CyclicBarrier barrier{kThreads};

    static const size_t kIterations = 256;

    auto runner_routine = [&barrier]() {
      for (size_t i = 0; i < kIterations; ++i) {
        barrier.PassThrough();
      }
    };

    std::vector<twist::th::thread> runners;

    for (size_t i = 0; i < kThreads; ++i) {
      runners.emplace_back(runner_routine);
    }

    for (auto& runner : runners) {
      runner.join();
    }
  }
}

RUN_ALL_TESTS()
